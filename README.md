# Fast (enough) Encryption in Javascript

This kit implements the RC4 algorithm and the Spritz variant, for:
- Generating pseudo-random key data
- Encrypting text (strings)

These algorithms have known and theoretical vulnerabilities. They're selected for use in web browser Javascript environments, because they're fast and reasonably lightweight, while providing "good enough" security for most use cases.

You can read more about [RC4 on Wikipedia](https://en.wikipedia.org/wiki/RC4).


## Known Vulnerabilities

RC4 is the basis of the WEP wireless encryption standard, which is now well-known to be broken. The methodology to break it typically requires collection of many thousands of samples (rather easy with wireless networks), and applies probabilistic techniques based on algorithmic biases in RC4, which expose likely key values. 

Spritz was drafted as an attempt to mitigate some of these problems in RC4, however it has undergone *less* analysis, testing, and review than the original RC4 algorithm, as Spritz has not been formally published at this time.

Considering the relatively siloed environment of web browsers, these were deemed to be tolerable vulnerabilities, believing the likelihood of cross-site scripting attack or (if the cipher gets transmitted) man-in-the-middle  attacks to be low enough that attackers would struggle to collect sufficient cipher text for cryptanalysis to yield useful key guesses.

Further, adaptations are enabled in this RC4 implementation to mitigate the more common attacks against the algorithm, such as enabling multiple iterations in calculating the RC4 key schedule. (The interfaces below automatically select 3 iterations.)

## Public Interface

The module registers in the `window` global as `window.ARC4`, hereafter identified as simply "ARC4".

The RC4 algorithm (and derivatives) use the same process for both decryption and encryption. The strings returned below will often include Unicode character sequences. 

When primes are required, they're pulled from an array of calculated primes, ranging between 1 and 16777215. 


```javascript

// Encrypt (or decrypt) a string, the simplest approach.
// This defaults to using the Spritz algorithm.
ARC4(String(keytext), String(plaintext))
// return String


// Encrypt a string (with some advanced options)
// More ciphers will be implemented in the future
CIPHER_ID = ARC4.ARC4_SPRITZ; // for the Spritz variant
CIPHER_ID = ARC4.ARC4_STANDARD; // for RC4 original
prime_seed = 11; // (default) offset into an array of known primes
ARC4.arc4(String(keytext), String(plaintext), CIPHER_ID, Number(prime_seed));
// return String


// is this value in our array of primes?
ARC4.isPrime(Number(value)) // return Boolean


// Produce a random prime value from the precalculated array.
ARC4.getPrime() // return Number


// A random integer between 0 and 0x7FFFFFFF
ARC4.getRandom() // return Number


// A prime number selected based on a hash of this browser's identification
ARC4.browserPrime() // return Number


// A prime number selected based on the current time (with tolerance)
// Time tolerance will "rotate" the key every time the epoch time, 
// divided by given tolerance, increases. In effect, if you calculate
// a time-prime with a 1 hour window, during *this* hour, each such
// request will produce the same value. The next hour will produce a
// different value.
tolerance = 3600; // one hour (in seconds)
ARC4.timePrime(Number(tolerance)) // return Number


// Generate a persistent cipher key based on this browser's identification
ARC4.browserKey() // return String


// As above, but incorporate a time tolerance on the key
ARC4.browserKey(Number(tolerance))


// Generate a random key data string.
// This string is calculated from a series of 32-bit numbers.
// The series is "seeded" with a few numbers, with which random prime
// numbers will be transposed using the methods above. Each iteration
// on the series XORs each existing value, with the lower 16 bits of the 
// following value (shifted up 16 bits), and then adds a random number.
// Random numbers will be transposed (segments * (iterations +1)) times.
// The loop will always run (iterations +1) cycles.
// The resulting string will always be (segments * 4) characters in length.
segments = 4; // (default) number of 32-bit chunks to calculate
iterations = 3; // (default) how many times to loop over all chunks
seed1 = 127; // (default) initial value for the first index of the output
seed2 = 17; // (default) initial value for the second index of the output
ARC4.randomKey(Number(segments), Number(iterations), Number(seed1), Number(seed2)
// return String


// Calculate a hash value for a given string, based on its RC4 key schedule
ARC4.hash(String(plaintext)) // return String


// Produce a pseudo-random generator keyed by random numbers.
// Each call to the resulting method will yield a value, ranging 0 to 255
// from the key schedule of RC4 with a randomly generated key data.
ARC4.random() // return Function


// Produce a key-based generator
// Each call to the resulting method will yield a value, ranging 0 to 255
// from the key schedule of RC4 with a given string as key data.
// This can be regarded as a pseudo-random number generator.
ARC4.generator(String(inputtext)) // return Function


```


## Building the Module

This project (currently) uses Google's Closure Compiler to compile its AMD modules (a dialect of AMD, anyway).

The [Makefile](./Makefile) simplifies it:

- `make all`  compiles the module and runs tests
- `make test` runs tests alone, using Node
- `make module` compiles the module `arc4.min.js` using Closure Compiler.




