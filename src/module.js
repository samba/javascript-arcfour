

define(function(require){

  var arc4 = require('./arc4');

  var _export = window['ARC4'] = function(keydata, plaintext){
    return arc4.arc4.call(arc4, keydata, plaintext, arc4.CIPHER_TYPE.ARC4_SPRITZ, 117);
  };

  _export['arc4'] = arc4.arc4;  // the crypto method itself


  // Key generatior methods
  _export['browserKey'] = function(){
    return arc4.toStringFromCode(arc4.browserKey.apply(arc4, arguments)); 
  };
  _export['randomKey'] = function(){
    return arc4.toStringFromCode(arc4.generateKey.apply(arc4, arguments));
  };


  // Key schedule applications
  _export['hash'] = arc4.hash;
  _export['generator'] = arc4.generator;
  _export['random'] = arc4.generator_random;

  // Prime number utilities
  _export['isPrime'] = arc4.isPrime;
  _export['getPrime'] = arc4.getPrime;
  _export['getRandom'] = arc4.getRandom;
  _export['browserPrime'] = arc4.browserPrime;
  _export['timePrime'] = arc4.timePrime;

  // String representation
  _export['hexchars'] = arc4.hexchars;
  _export['toString'] = arc4.toStringFromCode;

  // Algorithm variant identifiers
  _export['ARC4_STANDARD'] = arc4.CIPHER_TYPE.ARC4_STANDARD;
  _export['ARC4_SPRITZ'] = arc4.CIPHER_TYPE.ARC4_SPRITZ;

});
