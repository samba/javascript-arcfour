define(function(require, exports, module){

	var arc4 = require('./arc4');
	var shiftstring = require('./shiftstring');
	var util = require('./util');

	function hex(s){ return util.hex(s).join(' ') }
	function hexarr(a){
		var i = a.length, r = [];
		while(i--){ r[i] = Number(a[i]).toString(16); }
		return r.join(' ');
	}

	function arc4hex(key, content){
	  return util.hex(arc4.arc4(key, content));
	}

	function testKnownPrimePublic(){
	  var value = arc4.getPrime();
	  return [ arc4.isPrime(value) ? 'prime': 'not prime', value ];
	}

	function testARC4hash(){
		var preamble = 'We hold these truths to be self-evident';
		var hash = arc4.hash(preamble);
		console.info('Hash of preamble: ', hash.length, hash);
	}


	function testARC4Spritz(){
	  var preamble = 'We hold these truths to be self-evident';
	  var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890:<>@#$%^&*()!';
	  var plain = 'pedia';
	  var keydata = "Wiki";

	  var plaintext_use = preamble;

	  var encoded = arc4.arc4(keydata, plaintext_use, arc4.CIPHER_TYPE.ARC4_SPRITZ, 27);
	  var decoded = arc4.arc4(keydata, encoded, arc4.CIPHER_TYPE.ARC4_SPRITZ, 27);


	  console.info('Spritz plaintext:  ', hex(plaintext_use));
	  console.info('Spritz ciphertext: ', hex(encoded));
	  console.info('Spritz decoded:    ', hex(decoded));

	  console.assert(decoded == plaintext_use, 'Spritz shuffle failed');
	  /* if success */ return ('Spritz shuffle passed');
	}



	function testARC4Classic(){
	  var preamble = 'We hold these truths to be self-evident';
	  var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890:<>@#$%^&*()!';
	  var plain = 'pedia';
	  var keydata = "Wiki";

	  var plaintext_use = preamble;

	  var encoded = arc4.arc4(keydata, plaintext_use);
	  var decoded = arc4.arc4(keydata, encoded);


	  console.info('ARC4 plaintext:  ', hex(plaintext_use));
	  console.info('ARC4 ciphertext: ', hex(encoded));
	  console.info('ARC4 decoded:    ', hex(decoded));

	  console.assert(decoded == plaintext_use, 'ARC4 shuffle failed');
	  /* if success */ return ('ARC4 shuffle passed');
	}

	function testTimePrime(){
		var q = arc4.timePrime(3600);
		console.info("Current time-based prime", q);
	}

	function testStringBitShift(){
		var sample_string = 'We hold these truths to be self-evident';
		var fromstr = shiftstring.fromstr;
		var tostr = shiftstring.tostr;
		var shifted = shiftstring.lshift(fromstr(sample_string), 10);
		var result = shiftstring.rshift(shifted, 10);
		console.info('Shifted plain: ', hex(sample_string));
		console.info('Shifted cipher:', hexarr(shifted));
		console.info('Shifted result:', hexarr(result));
		console.assert(tostr(result) == sample_string, "String bit shift failed");
	}


	function main(){

		// var abc = arc4.schedule("ABC");
		// console.info("Sample ABC", abc.length, abc.slice(0, 15));

		console.info(testARC4hash() || '');

		// console.info(arc4.hexencode(arc4.generateKey(5)));
		console.info(testARC4Spritz() || '');
		console.info(testARC4Classic() || '');

		console.info(testTimePrime() || '');
		console.info(testStringBitShift() || '');

  	}


	exports.testKnownPrimePublic = testKnownPrimePublic;
	exports.main = main;

});
