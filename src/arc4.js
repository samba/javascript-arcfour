

/* More reference...
    https://people.csail.mit.edu/rivest/pubs/RS14.pdf
 */


define(function(require, exports, module){

  /** @enum {number} */
  var CIPHER_TYPE = {
    ARC4_STANDARD: 0,
    ARC4_SPRITZ: 1
  };

  /** Convert an array of numbers to string...
   * @param {Array.<number>} chars
   * @return {string}
   */
  function toStringFromCode(chars){
    return String.fromCharCode.apply(String, chars);
  }


  // Sieve of Erasthones; generates a list of primes up to a limit.
  function primeSieve(max, primes_out){
    var sieve = new Array(max), c = 0, j = 2, i;

    // Start with 2; values 1 and 0 are distinct and easily managed in other code.
    for(i = 2; i < max; ++i){
      if(!sieve[i]){ // If |i| has not been marked as non-prime
        primes_out[c++] = i; // Add |i| to the primes
        for(j = i << 1; j < max; j += i){
          sieve[j] = true; // Mark all multiples of primes as non-prime
        }
      }
    }

    return primes_out;
  }

  // Euclid's method for finding GCD.
  function greatestCommonDenominator(a, b){
    var t;
    while(b !== 0){
      t = a;
      a = b;
      b = t % b;
    }
    return a;
  }

  /** Determine whether "a" is relatively prime to "b", i.e. that their
    * mutual greatest common denominator is 1.
    * @param {number} a
    * @param {number} b
    * @return {boolean}
    */
  function isRelativelyPrime(a, b){
    return greatestCommonDenominator(a, b) === 1;
  }

  /** Produce a large, positive pseudo-random integer
   * @return {number}
   */
  function getBigRandom(){
    return (Math.random() * 0x7FFFFFFF) << 0;
  }

  function randint(a, b){
    return Math.round(Math.random() * (b - a)) + a;
  }


  /** Assemble a very large list of prime numbers
   * @type {Array.<number>}
   */
  var primes = [];

  /** Procure a (known) prime number from our precalculated list
   * @param {number} index
   * @return {number}
   */
  function getPrime(index){
    if(!primes.length){ // Initialize the list if needed.
      primeSieve(0x00FFFFFF, primes);
      primes.splice(0, 50); // remove lower 50
    }
    return primes[index % primes.length];
  }

  /**
   * @param {...string} var_strings
   * @return {number}
   */
  function hashStringToNumber(var_strings){
    var i = arguments.length, val = 255, k;
    while(i--){
      k = (typeof arguments[i] == 'string') && arguments[i].length;
      while(k--){
        val = (val << 5) ^ arguments[i].charCodeAt(k) - k;
      }
    }
    return val;
  }

  /** Retrieve a prime number based on the browser's identification
   * @return {number}
   */
  function browserPrime(){
    var hash = hashStringToNumber(
      window.navigator.userAgent,
      window.navigator.vendor,
      window.navigator.language
    );
    return getPrime(hash);
  }

  /** Retrieve a prime number based on current time
   * @param {number=} precision_seconds
   * @return {number}
   */
  function timePrime(precision_seconds){
    var time = ((new Date).getTime() / 1000);
    var _precision = Number(precision_seconds || 5);
    var index = Math.round(time / _precision) * _precision;
    return getPrime(index);
  }


  // Is the given number in our list of primes?
  function isPrime(n){
    return (~ _indexOf.call(primes, n));
  }

  /** Seek an item in a list
   * @this {Array}
   * @param {?} n
   * @return {number}
   */
  function _indexOf(n){
    var i = this.length;
    while(i--) if(n === this[i]) break;
    return i;
  }

  /** Procure a _random_ prime number from the list
   * @return {number}
   */
  function getPrimeRandom(){
    return getPrime(getBigRandom());
  }

  function shift_rotate_left(value, bits){
    return ((value << bits) | (value >>> (32-bits)));
  }

  function shift_rotate_right(value, bits){
    return ((value >>> bits) | (value << (32-bits)));
  }


  /** Generate a key based sequential hashing of random values
   * Length of keys is arbitrarily defined in multiples of 4 output characters (32 bits per key field).
   * @param {number=} iterations
   * @param {number=} key_len  result key length (# of 32-bit chars) default=4
   * @param {number=} prime0   seed prime
   * @param {number=} prime1   seed prime
   * @return {Array.<number>}
   */
  function generateKey(key_len, iterations, prime0, prime1){
    var i = Number(iterations || 3) +1,
        key_len_actual = Number(key_len || 4), last;

    // Seed the hash sequence
    var r = [ Number(prime0 || 127), Number(prime1 || 17), 0, timePrime(1) ];

    var z = []; // result output
    var q; // sentinel


    while(i--){ // For the given iteration count (i.e. how many times to loop over the entire set)

      // Hash the entire length of the key (32-bit fields)
      q = key_len_actual;
      while(q--){
        last = (r[(q+1) % key_len_actual] || 0); // The next (index +1) value in the set, rolling to 0 on the last.
        r[q] = (r[q] ^ ((last)<<16)) + getPrimeRandom(); // Hash a new prime number with the one following, and the prior state of this one.
      }

    }


    // Prepare the character representation (8-bit chunks)
    i = key_len_actual;
    while(i--){
      z.push(
        (r[i] & 0xFF000000) >>> 24,
        (r[i] & 0xFF0000) >>> 16,
        (r[i] & 0xFF00) >>> 8,
        r[i] & 0xFF
      );
    }

    return z; // The final sequence of 8-bit chunks.
  }


  /** Simple numeric XOR value on string
   * @param {string} basetext
   * @param {number} keyval
   * @return {string}
   */
  function xor(basetext, keyval){
    var i = basetext.length, r = [];
    while(i--){
      r[i] = basetext.charCodeAt(i) ^ (keyval);
    }
    return toStringFromCode(r);
  }




  /** Produce a "key schedule" from input data, in essence a hash.
   * @param {string} keydata
   * @param {number=} iterations
   * @param {number=} arithmetic
   * @return {Array.<number>}
   */
  function ARCFOUR_keyschedule(keydata, iterations, arithmetic){
    var actual_arithmetic = Number(arithmetic || 256);
    var i = actual_arithmetic, z, j = 0, c = 0,
      schedule = new Array(actual_arithmetic - 1);

    (iterations) || (iterations = 1);

    while(i--)
      schedule[i] = i;

    while(iterations--){
      // console.info('Schedule iteration', iterations, schedule.length);
      for(i = 0; i < actual_arithmetic; i++){
        j = (j + schedule[i] + (keydata.charCodeAt((c++) % keydata.length)));
        j = j % actual_arithmetic;
        z = schedule[i];
        schedule[i] = schedule[j];
        schedule[j] = z;
      }
    }

    return  schedule;
  }



  /** Generate a key for this particular browser, with an optional time-expiration window.
   * @param {number=} time_precision
   * @param {number=} keyscope
   * @return {Array.<number>}
   */
  function browserKeySimple(time_precision, keyscope){
    var parts = [
      window.navigator.userAgent,
      window.navigator.vendor,
      window.navigator.language
    ];

    var timewindow = (time_precision || 0) && timePrime(time_precision);
    var primeKey = browserPrime() + timewindow;
    return ARCFOUR_keyschedule(parts.join('$'), primeKey % 32, Number(keyscope || 255));
  }



  // NOTE: this only works properly for values < 256.
  function hexchars(values){
    var i = values.length, result = [];
    while(i--){
      result[i] = ((values[i] >> 4) ? '' : '0') + Number(values[i] & 0xFF).toString(16);
    }
    return result.join('');
  }





  /** Construct a hash string for the given key data
   * @param {string} keydata
   * @param {number=} hash_len
   * @return {string}
   */
  function ARCFOUR_hash(keydata, hash_len){
    var sched = ARCFOUR_keyschedule(keydata, 2);
    var result = [], i = sched.length;
    var result_len = (hash_len || 16);
    while(i--){
      result[i % result_len] = (result[i % result_len] ^ sched[i]) % 0xFF;
    }
    return hexchars(result);
  }


  /** ARCFOUR/Spritz state
   * @constructor
   * @param {string|Array.<number>} keydata
   * @param {number=} prime_seed (only applicable to Spritz algorithm)
   */

  function ARCFOUR(keydata, prime_seed){
    this.keydata = keydata;
    if (typeof keydata == 'string'){
      this.keyschedule = ARCFOUR_keyschedule(keydata, 3);
    } else {
      this.keyschedule = keydata;
    }
    this.prime = prime_seed || getPrimeRandom();
    this.resetState();

  }

  (function(proto){

    var arithmetic = 256;

    var Ciphers = [];
    Ciphers[CIPHER_TYPE.ARC4_STANDARD] = RC4_getNextKeyValue;
    Ciphers[CIPHER_TYPE.ARC4_SPRITZ] = Spritz_getNextKeyValue;



    // Internal
    function Spritz_getNextKeyValue(state, prime, schedule){
      var temp;
      state.i = (state.i + prime) % arithmetic;
      state.j = (state.k + schedule[(state.j + schedule[state.i]) % arithmetic]) % arithmetic;
      state.k = (state.k + state.i + schedule[state.j]) % arithmetic;

      // Swap
      temp = schedule[state.i];
      schedule[state.i] = schedule[state.j];
      schedule[state.j] = temp;

      // Prepre key value
      state.z = schedule[(state.z + state.k) % arithmetic];
      state.z = schedule[(state.i + state.z) % arithmetic];
      state.z = schedule[(state.j + state.z) % arithmetic];

      return state.z;
    }

    // Internal
    function RC4_getNextKeyValue(state, prime, schedule){
      var temp;
      state.i = (state.i + 1) % arithmetic;
      state.j = (state.j + schedule[state.i]) % arithmetic;

      // Swap
      temp = schedule[state.j];
      schedule[state.j] = schedule[state.i];
      schedule[state.i] = temp;

      return schedule[(schedule[state.i] + schedule[state.j]) % arithmetic];

    }


    proto.resetState =  function(){
      this.$state = { i: 0, j: 0, k: 0, z: 0 };
    };

    /** Procure a key value from the key schedule based on current state.
     * @param {function(Object, number, Array.<number>):number} method
     * @return {number}
     */
    proto.next = function(method){
      return method(this.$state, this.prime, this.keyschedule);
    };


    /** Convert a string, possibly selecting a cipher algorithm by ID
     * @param {string} inputstream
     * @param {CIPHER_TYPE=} cipher_id
     * @return {string}
     */
    proto.cipher = function(inputstream, cipher_id){
      var method = Ciphers[cipher_id || CIPHER_TYPE.ARC4_STANDARD];
      for(var output = [], z = 0; z < inputstream.length; z++){
        output[z] = inputstream.charCodeAt(z) ^ this.next(method);
      }
      this.resetState();
      return toStringFromCode(output);
    };


    /** A pseudo-random number generator based on the key schedule
     * @param {CIPHER_TYPE=} cipher_id
     * @return {function():number}
     */
    proto.generator = function(cipher_id){
      var method = Ciphers[cipher_id || CIPHER_TYPE.ARC4_STANDARD];
      var that = this;
      return function(){
        return that.next(method);
      };
    };


  }(ARCFOUR.prototype));


  /** Shortcut accessor method for arc4 conversion
   * @param {string} keydata
   * @param {string} inputstream
   * @param {CIPHER_TYPE=} cipher_id
   * @param {number=} prime_seed_index
   * @return {string}
   */
  function arc4(keydata, inputstream, cipher_id, prime_seed_index){
    var prime = cipher_id && getPrime(Number(prime_seed_index || 11));
    var handler = (new ARCFOUR(keydata, prime));
    return handler.cipher(inputstream, cipher_id);
  }


  /** Shortcut accessor method for arc4 key-schedule generator, key-based
   * @param {string|Array.<number>} keydata
   * @param {CIPHER_TYPE=} cipher_id
   * @param {number=} prime_seed_index
   * @return {function():number}
   */
  function arc4generator(keydata, cipher_id, prime_seed_index){
    var prime = cipher_id && getPrime(Number(prime_seed_index || 11));
    var handler = (new ARCFOUR(keydata || browserKeySimple(0), prime));
    return handler.generator(cipher_id);
  }


  /** Shortcut accessor method for arc4 key-schedule generator, random seed
   * @param {CIPHER_TYPE=} cipher_id
   * @param {number=} prime_seed_index
   * @return {function():number}
   */
  function arc4generator_random(cipher_id, prime_seed_index){
    var random_data = generateKey(128, 3, timePrime(1), browserPrime());
    return arc4generator(random_data, cipher_id, prime_seed_index);
  }


  exports.isPrime = isPrime;


  exports.getPrime = getPrimeRandom;
  exports.getRandom = getBigRandom;

  exports.arc4 = arc4;
  exports.generator = arc4generator;
  exports.generator_random = arc4generator_random;
  exports.hash = ARCFOUR_hash;
  exports.schedule = ARCFOUR_keyschedule;
  exports.construct = ARCFOUR;
  exports.generateKey = generateKey;

  exports.browserPrime = browserPrime;
  exports.timePrime = timePrime;

  exports.browserKey = browserKeySimple;

  exports.hexchars = hexchars;

  exports.shift_rotate_left = shift_rotate_left;
  exports.shift_rotate_right = shift_rotate_right;

  exports.CIPHER_TYPE = CIPHER_TYPE;

  exports.toStringFromCode = toStringFromCode;

});





