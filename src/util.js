/* Utility methods... mostly for testing/debugging */

define(function(require, exports, module){



	/** Map a string like an array...
	 * @this {string}
	 * @param {function(number, string, number)} fn
	 * @param {boolean=} stringify
	 * @return {Array.<string>|string}
	 */
	function _mapstr(fn, stringify){
	  var i = this.length, n = [];
	  while(i--){
	    n[i] = fn.call(this, i, this.charAt(i), this.charCodeAt(i));
	  }
	  return stringify ? String.fromCharCode.apply(String, n) : n;
	}

	/** Encode a string in its hexidecimal form, byte for byte
	 * @param {string} string
	 * @return {Array.<string>|string}
	 */
	function hexencode(string){
	  return  _mapstr.call(string, function(index, cr, code){
	    return (code < 16 ? '0' : '') + Number(code).toString(16);
	  });
	}

	exports.hex = hexencode;
	exports.mapstr = _mapstr;

});