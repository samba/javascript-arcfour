define(function(require, exports, module){

  function tostr(chars){
    return String.fromCharCode.apply(String, chars);
  }

  function fromstr(text){
    var r = [], i = text.length;
    while(i--) r[i] = text.charCodeAt(i);
    return r;
  }

  function lshift(values, bits){
    var max = values.length -1;
    var first = values[0] >>> (32 - bits);
    var i, next, result = [];
    for(i = 0; i < max; i++){
      next = values[i +1];
      result[i] = (values[i] << bits) | (next >>> (32 - bits));
    }
    result[max] = (values[max] << bits) | first;
    return result;
  }

  function rshift(values, bits){
    var max = values.length -1;
    var mask = (1 << bits) -1;
    var last = values[max] & mask;
    var i = values.length, result = [];
    var upper = 0;
    while(i--){
      result[i] = (values[i] >>> bits);
      upper = (i > 0 ? ((values[i - 1] & mask)) : last);
      result[i] |= (upper << (32 - bits));
    }
    return result;
  }

  exports.lshift = lshift;
  exports.rshift = rshift;

  exports.fromstr = fromstr;
  exports.tostr = tostr;


  exports.ashex = function(text){
    var r = [], i = 0;
    for(i = 0; i < text.length; i++){
      r[i] = text.charCodeAt(i).toString(16);
    }
    return r;
  };


})