# Notes

## Future integration

- More RC4 variants
- ChaCha20


## References
- [ChaCha20](https://en.wikipedia.org/wiki/Salsa20#ChaCha_variant)
- [ChaCha20 in Javascript](https://gist.github.com/devi/da696f47865605e5f6ed)