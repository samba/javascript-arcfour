COMPILER=/opt/closure-compiler/compiler.jar
JAVASCRIPT_VERSION ?= ECMASCRIPT3
COMPILATION_LEVEL ?= ADVANCED_OPTIMIZATIONS
WARNING_LEVEL ?= VERBOSE
FLAG_FILE ?= ./compile-flags-prod.txt


GIT_COMMIT = $(shell git log -1 "--pretty=format:%H")
GIT_BRANCH = $(shell git describe --contains --all HEAD)
GIT_STATUS = $(shell git status -sb --untracked=no | wc -l | awk '{ if($$1 == 1){ print "clean" } else { print "pending" } }')
TIMESTAMP = $(shell date +"%Y%m%d.%H%M%S")

.PHONY: all test build clean

all: arc4.min.js test

test: build/compiled/main.js
	node $^


build: build/compiled/main.js

debug: build/compiled/main.js
	node-debug $^

module: arc4.min.js

build/compiled/module.js: src/arc4.js

build/compiled/main.js: src/arc4.js src/test.js src/util.js src/shiftstring.js

arc4.min.js: src/header.txt build/compiled/module.js
	cat $^ | sed 's@{commit}@$(GIT_COMMIT)@; s@{status}@$(GIT_STATUS)@; s@{branch}@$(GIT_BRANCH)@; s@{time}@$(TIMESTAMP)@;' > $@

clean:
	rm -rvf build/compiled


build/compiled/%.js: src/%.js
	mkdir -p `dirname $@`
	java -jar ${COMPILER} \
		--warning_level=${WARNING_LEVEL} \
		--transform_amd_modules \
		--process_common_js_modules \
		--process_closure_primitives \
		--process_jquery_primitives \
		--common_js_entry_module=$< \
		--flagfile=$(FLAG_FILE) \
		--js_output_file="$@" \
		--create_source_map='%outname%.map' \
		$< $(filter-out $<, $^)
ifeq ($(DO_SOURCEMAP),true)
	printf "//@ sourceMappingURL=%s\n" "`basename $@.map`" >> $@
	# Source mapping requires related sources be copied in... these should be stripped for live deployment.
	@$(foreach var, $^, mkdir -p "build/`dirname $(var)`"; cp -v $(var) "build/$(var)"; )
endif


