#!/usr/bin/env python

import sys

def primeShuffle(source, prime):
    _len = len(source)
    result = [ c for c in source ]
    for i in range(0, _len, 1):
        newpos = (i * prime) % _len
        result[newpos] = unichr(ord(source[i]) ^ prime)
    return unicode(u''.join(result))

def primes(optimus):
    sieve = [ True ] * optimus
    sieve[0] = sieve[1] = False
    for i, isprime in enumerate(sieve):
        if isprime:
            yield i
            for n in xrange(i*i, optimus, i):
                sieve[n] = False

def testPrimeShuffle(text, maximum):
    _primes = list(primes(maximum or 500))
    for i in _primes:
        for j in _primes:
            if primeShuffle(primeShuffle(text, i), j) == text:
                print '{0}\t{1}\t{2}\t"{3}"'.format(i, j, len(text), text)



def main(mode, source, prime):
    if mode == 'shuffle':
        print u'# Applying {0} to "{1}"\n'.format(prime, unicode(source))
        result = unicode(primeShuffle(unicode(source), int(prime)))
        print 'Result "{0}"\n'.format(repr(result))
    elif mode == 'test':
        testPrimeShuffle(source, int(prime))






if __name__ == '__main__':
    main(sys.argv[1], sys.argv[3], int(sys.argv[2]))
